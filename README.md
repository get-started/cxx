# Sample C++ application

This tutorial show how to build a C++ application.

Directory structure:

```
./                   - root directory
    calc_crc32/        - main executable file, e.g. calc_crc32.exe
        *.cpp            - source files
        CMakeLists.txt   - build script
    crc32/             - static library with the application logic
        *.cpp, *.hpp     - source files
        *_test.cpp       - unit tests
        CMakeLists.txt   - build script
    tests/*            - auxiliary files for unit tests
    third_party/*      - third party libraries
    CMakeLists.txt     - main build script
```

## Required build tools

### cmake 2.8.12+
CMake 3.x would be preferable, but many environments still have old cmake.  
So if you don't need the new features of CMake 3.x, don't require it in CMakeLists.txt.

### A C++ compiler
MSVC or MinGW on Windows, g++ or clang++ on *nix.

## Setting things up

Create a directory for binary files, and run `cmake /path/to/source` from it.  
Or, just run `.\configure.bat`.

## Build and test

Depending on which build system you use, build the .sln file, run `make` or `ninja`.  
Tests will be executed in a post-build step.